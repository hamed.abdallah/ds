FROM openjdk:7

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} myproject-0.0.1-SNAPSHOT.jar
Expose 8080
ENTRYPOINT [run.sh]
